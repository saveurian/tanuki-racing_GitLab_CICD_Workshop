# Link to test answer on LeetCode: https://leetcode.com/problems/number-of-segments-in-a-string/description/?envType=featured-list

# Number of Segments in a String Problem: We are given a string s, return the number of segments in the string where we define a segment as a contiguous sequence of non-space characters. Provide two different solutions for us to choose from.

