### We will take a look at the SBOM report that our scanners created as well as see the various licenses that our scanners detected

### Theme

With the uptick of major security breaches hitting headlines many governments have started to require their developers and those that work with the Government to provide detailed reports on the dependencies of their applications. This section will show you how GitLab creates these reports for you.

# Step 1: Review & Download SBOM report

1. Using the left hand navigation menu click through **Secure \> Dependency list** to view all of the dependencies that are directly and indirectly included in your application.
  
2. Click through a few of the pages and notice the components that are all directly/indirectly included in your application.
  
3. Next to export these findings in the CycloneDX format we will use the left hand navigation menu to click through **Build > Pipelines** and take a look at the pipeline most recently ran against **main**

4. Next click the ***download*** icon next to that pipeline listing and then click the container_scanning:cyclonedx artifact. This will download your SBOM report in CycloneDX format. To learn more about CycloneDX format go [here](https://cyclonedx.org/)

> [See how you could have used GitLab to detect log4j](https://about.gitlab.com/blog/2021/12/15/use-gitlab-to-detect-vulnerabilities/)

# Step 2: License Compliance

1. We can also see the licenses detected in our project by using the left hand navigation menu to click back through the **Secure \> Dependency list**. To better see the included licenses select **Packager** in the top right filter (you may have to reverse the filter as well).
  
2. Lets say we decided we want to prevent the use of the LGPL-3.0 License. Using the left hand navigation menu click through the **Secure \> Policies** then click **New policy**.
  
3. Click **Select policy** under **Merge Request Approval Policy**
  
4. In the **New Merge Request Approval Policy form** that appears, at the top change from _Rule mode_ to _.yaml mode_ then replace the existing yaml with our new config below:

 ```
type: approval_policy
name: Deny LGPL-3.0 License
description: Denying unwanted license
enabled: true
rules:
  - type: license_finding
    match_on_inclusion_license: true
    license_types:
      - LGPL-3.0
    license_states:
      - newly_detected
      - detected
    branch_type: protected
actions:
  - type: require_approval
    approvals_required: 1
    user_approvers_ids:
      - 10994493
approval_settings:
  block_branch_modification: true
  prevent_pushing_and_force_pushing: false
```

5. Merge the new merge request into the existing security policy project.
  
6. _Remember to go back to your project using the breadcrumb, clicking on your group, then clicking on your project._
  
7. Now we will run a new pipeline for a MR, a new approval rule based on this license compliance policy will be added to prevent any software using the LGPL-3.0 license from being merged and the security bot will notify you that you have a policy violation.